function clock() {
    var d = new Date();
    var hr = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds();
    var hour = document.getElementById("hour");
    var minutes = document.getElementById("minutes");
    var seconds = document.getElementById("seconds");
    hour.textContent = "";
    minutes.textContent="";
    seconds.textContent="";
    hour.appendChild(document.createTextNode(hr + " : "));
    minutes.appendChild(document.createTextNode(min  + " : "));
    if (sec < 10){
        seconds.appendChild(document.createTextNode("0" + sec ));
    }
    else {
        seconds.appendChild(document.createTextNode(sec));
    }

}

var interval;
function start() {
    interval = setInterval(clock,  1000);
}

function stop() {
    clearInterval(interval);
}


document.getElementById("start").addEventListener("click", start);
document.getElementById("stop").addEventListener("click", stop);
