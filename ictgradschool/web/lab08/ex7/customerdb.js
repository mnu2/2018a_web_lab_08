var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },

	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];

function createRows() {
	for(var i = 0; i < customers.length; i++){
        var a = document.createElement('tr');
        document.getElementsByTagName('tbody')[0].appendChild(a);
}
}

function fillRows(key){
	for (var i = 0; i < customers.length; i++) {
        var cell = document.createElement('td');
        var value = document.createTextNode(customers[i][key]);
        cell.appendChild(value);
        cell.style.border='1px solid';
        cell.style.padding='5px';
        document.getElementsByTagName('tr')[i + 2].appendChild(cell);
    }
}

function createHead(){
    var head = document.createElement('tr');
    var hcell = document.createElement('th');
    hcell.setAttribute('colspan', 5);
    hcell.innerHTML="Customer Data";
    head.appendChild(hcell);
    hcell.style.border='1px solid';
    document.getElementsByTagName('thead')[0].appendChild(head);
    var row = document.createElement('tr');
    var name = document.createTextNode("Name");
    var gender = document.createTextNode('Gender');
    var DOB = document.createTextNode('Birth Year');
    var membersince = document.createTextNode('Member Since');
    var rentals = document.createTextNode('Number of Rentals');
    for (var i = 0; i<5; i++){
        var element = document.createElement('th');
        row.appendChild(element);
    }
    document.getElementsByTagName('thead')[0].appendChild(row);

    var cat = document.getElementsByTagName('th');
    cat[1].appendChild(name);
    cat[2].appendChild(gender);
    cat[3].appendChild(DOB);
    cat[4].appendChild(membersince);
    cat[5].appendChild(rentals);

    for (var i = 1; i< 6; i++){
        cat[i].style.border='1px solid';
        if(i%2===1){
            cat[i].style.backgroundColor='lavender';
        }
    }


}


createHead()
createRows();
fillRows('name');
fillRows('gender');
fillRows('year_born');
fillRows('joined');
fillRows('num_hires');

document.getElementsByTagName('table')[0].style.borderCollapse='collapse';
document.getElementsByTagName('table')[0].style.borderStyle='solid';


var data = document.getElementsByTagName('td');

for (var i = 0; i < data.length; i++){
    if ((i%5)%2===0) {
        data[i].style.backgroundColor = "lavender";
    }
}


var num_males = 0;
var num_females = 0;
var young = 0, middle = 0, old =0;
var bronze = 0, silver = 0, gold = 0;
for (var i = 0; i < customers.length; i++){
   switch(customers[i].gender){
       case ("male"):
           num_males++;
       default:
        num_females++;
    }

    if (parseInt(customers[i].year_born) > 1987){
       young++;
    }
    else if (parseInt(customers[i].year_born) > (2018 - 65)){
       middle++;
    }
    else {
       old++;
    }
    var perweek = parseInt(customers[i].num_hires)/(52*(2018 - parseInt(customers[i].joined)));
    if (perweek > 4){gold++;}
    else if (perweek > 1){silver++;}
    else {bronze++;}
}

var summary = document.createElement('div');
summary.innerHTML="<h3>This table represents:</h3>";
var ul = document.createElement('ul');
for (var i = 0; i < 3; i++){
    var k = document.createElement('li');
    ul.appendChild(k)
}
summary.appendChild(ul);
document.getElementsByTagName('body')[0].appendChild(summary);
var stats = document.getElementsByTagName('li');
stats[0].innerHTML = num_males + " males and " + num_females + " females,";
stats[1].innerHTML = young + " people under 31, " + middle + " people between 31 and 64, and " + old + " people over 65";
stats[2].innerHTML = gold + " Gold customers, " + silver + " Silver customers, and " + bronze + " Bronze customers.";

